Vue.filter('html', function (value) {
  return value.replace(/(<([^>]+)>)/ig,"")
})

Vue.component('vue-posts', {
  template: `
    <div>
      <h1>Vue Posts</h1>
      <ul class="list-unstyled">
        <li v-for="post in posts">
          <a :href="post.link"><img :src="post.featured_image" /></a>
          <h2>{{ post.title.rendered }}</h2>
          <p>{{ post.excerpt.rendered | html }}</p>
          <br/>
          <p>Categories: {{ post.cat_names }}</p>
          <p>Tags: {{ post.tag_names }}</p>
        </li>
      </ul>
    </div>
  `,
  created() {
      this.getPosts();
  },

  data() {
      return {
        posts: []
      };
    },

  methods: {
      getPosts() {
        var self = this;
        $.ajax({
          url: 'http://localhost/wpdev/wp-json/wp/v2/posts?_embed',
          type: 'GET',
          data: {
            format: 'json'
         },
          success: function (data) {
              self.posts = data;
              console.log(data);
          },
          error: function (error) {
              console.log(JSON.stringify(error));
          }
        });

      }
  }
});

new Vue({
  el: '#main'
});
