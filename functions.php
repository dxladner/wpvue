<?php
/**
 * wpvue functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wpvue
 */

if ( ! function_exists( 'wpvue_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wpvue_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wpvue, use a find and replace
	 * to change 'wpvue' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wpvue', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'wpvue' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wpvue_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'wpvue_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpvue_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wpvue_content_width', 640 );
}
add_action( 'after_setup_theme', 'wpvue_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wpvue_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wpvue' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wpvue' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wpvue_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wpvue_scripts() {
	wp_enqueue_style( 'wpvue-style', get_stylesheet_uri() );
	wp_enqueue_style( 'wpvue-bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'wpvue-custom-css', get_template_directory_uri() . '/css/custom.css' );

	wp_enqueue_script( 'wpvue-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'wpvue-vue-js', get_template_directory_uri() . '/js/vue.js', array(), '2.1.0', true );
	wp_enqueue_script( 'wpvue-jquery-js', get_template_directory_uri() . '/js/jquery.min.js', array(), '2.1.0', true );

	wp_enqueue_script( 'wpvue-vue-router-js', get_template_directory_uri() . '/js/vue-router.js', array(), '2.1.3', true );
	wp_enqueue_script( 'wpvue-axios-js', get_template_directory_uri() . '/js/axios.min.js', array(), '0.15.3', true );
	wp_enqueue_script( 'wpvue-vue-resource-js', get_template_directory_uri() . '/js/vue-resource.js', array(), '1.0.3', true );

	wp_enqueue_script( 'wpvue-vue-posts', get_template_directory_uri() . '/js/components/vue-posts.js', array(), '1.0', true );


	wp_enqueue_script( 'wpvue-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpvue_scripts' );

//Get image URL
function get_thumbnail_url($post){
    if(has_post_thumbnail($post['id'])){
        $imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post['id'] ), 'full' ); // replace 'full' with 'thumbnail' to get a thumbnail
        $imgURL = $imgArray[0];
        return $imgURL;
    } else {
        return false;
    }
}
//integrate with WP-REST-API
function insert_thumbnail_url() {
     register_rest_field(
			 	'post',
        'featured_image',  //key-name in json response
         array(
           'get_callback'    => 'get_thumbnail_url',
           'update_callback' => null,
           'schema'          => null,
           )
       );
     }
//register action
add_action( 'rest_api_init', 'insert_thumbnail_url' );

// get categories name by post
function get_category_names($post){
		if(has_category('', $post['id']))
		{
				$catNamesArray =  get_the_category( $post['id'] );

				foreach ($catNamesArray as $catsName)
				{
					$cattys[] = $catsName->name;
					$final_cats = implode(',', $cattys);
				}
				return $final_cats;
    }
		else
		{
        return false;
    }
}
//integrate with WP-REST-API
function insert_category_name() {
     register_rest_field(
			 	'post',
        'cat_names',  //key-name in json response
         array(
           'get_callback'    => 'get_category_names',
           'update_callback' => null,
           'schema'          => null,
           )
       );
     }
//register action
add_action( 'rest_api_init', 'insert_category_name' );

// get tag name by post
function get_tag_names($post){
    if(has_tag('', $post['id']))
		{
        $tagNamesArray =  get_tags( $post['id'] );
				foreach ($tagNamesArray as $tagName)
				{
					$taggys[] = $tagName->name;
					$final_tags = implode(',', $taggys);
				}
				return $final_tags;
    }
		else
		{
        return false;
    }
}
//integrate with WP-REST-API
function insert_tag_names() {
     register_rest_field(
			 	'post',
        'tag_names',  //key-name in json response
         array(
           'get_callback'    => 'get_tag_names',
           'update_callback' => null,
           'schema'          => null,
           )
       );
     }
//register action
add_action( 'rest_api_init', 'insert_tag_names' );

/**
 * Load Bootstrap Menu.
 */
require get_template_directory() . '/inc/bootstrap-walker.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
